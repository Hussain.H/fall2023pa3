package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    App app = new App();
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldReturnSame(){
        assertEquals("Not the expected output",5, app.echo(5));
    }

    @Test
    public void shouldReturnOneMore(){
        assertEquals("Not the expected output", 6, app.oneMore(5));
    }
}
